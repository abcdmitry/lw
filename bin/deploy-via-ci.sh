#!/usr/bin/env bash

if [ "$BUILD_ENV" == "PROD" ]; then
    SSH_KEY=$PROD_SSH_KEY
    RSYNC_USER="lw"
    RSYNC_HOST="185.92.223.155"
    RSYNC_DIR="/public_html"
elif [ "$BUILD_ENV" == "STG" ]; then
    SSH_KEY=$STG_SSH_KEY
    RSYNC_USER="lw-stg"
    RSYNC_HOST="185.92.223.155"
    RSYNC_DIR="/public_html"
elif [ "$BUILD_ENV" == "QA1" ]; then
    SSH_KEY=$QA_SSH_KEY
    RSYNC_USER="lw-qa1"
    RSYNC_HOST="185.92.223.155"
    RSYNC_DIR="/public_html"
else
    echo "Unrecognized environment"
    exit 1
fi

mkdir ~/.ssh
echo "$SSH_KEY" > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa

cd dist/lw

echo "Uploading to server..."
rsync -azvhce "ssh -o StrictHostKeyChecking=no" --del --chmod=go-w,o+r \
    . $RSYNC_USER@$RSYNC_HOST:~$RSYNC_DIR

rm -rf ~/.ssh
