# Servers list application

This application is intended to fetch servers list from the backend and display it to the user, allowing to sort, filter and search through the entire list.

Since it's a front-end task, this application relies heavily on the front-end logic. Of course in the real-world applications data is supposed to be normalized at the backend side, as well as the backend should provide features like pagination, sorting and search via API parameters.

For the purposes of this task, let's assume the constraint that backend can only provide a json with unordered list of servers and we have to simulate back-end logic inside the application.

## Servers and environments
* PROD: https://lw.lukashin.ru
* STG: https://lw-stg.lukashin.ru
* QA1: https://lw-qa1.lukashin.ru

It is easy to get confused with environments, let's make it clear:
* **Build environment (target)** - related to `angular-cli` build process. Can be `development` and `production` only. `development` preserves debugging information (source maps). `production` is for highly optimized bundles.
* **Application environment** - This is where you should actually configure environment-related variables, like API endpoint urls. Look at `_environments` folder. 
* **CI environment** - Different server addresses for deployment and related things: special build commands if needed, private variables, like SSH-keys.

Deployments to all servers are built in `production` mode (they do not contain source-maps).

`QA1` and `STG` servers work in `staging` environment (they do not use real API endpoints). `PROD` works in `production` environment.

When you are developing locally, application bundles contain source-maps and debugging information, and application works in `local` (aka `development`) environment. 

## Local development

In order to develop locally you need:
* [Node.js](https://nodejs.org/en/download/)
* [Yarn package manager](https://yarnpkg.com/en/docs/install) - optional, but recommended. Use `npm` otherwise

### Quickstart
1. Check out the repository from git
1. Run `yarn`
1. Run `yarn start`

## List of commands

* `yarn` to install dependencies
* `yarn start` for a local dev server. The app will automatically reload if you change any of the source files.
* `yarn test` locally run unit tests via [Karma](https://karma-runner.github.io). Chrome browser is used
* `yarn test:ci` execute tests in CI environment
* `yarn run e2e` locally execute the end-to-end tests via [Protractor](http://www.protractortest.org/). Chrome browser is used
* `yarn run build` to build development version
* `yarn run build:prod` to build production version
* `yarn run build:stg` to build staging version

These commands are based on `ng` tool (see `package.json` `scripts` section).

For the complete reference of available commands check [Angular CLI documetation](https://github.com/angular/angular-cli/wiki)

## JetBrains IDE
If you use JetBrains family IDE, make sure you have following plugins installed:
1. `AngularJS`: supporting both AngularJS1 and Angular2+
2. `Editorconfig`: overrides IDE defaults with options specified in .editorconfig. It misses from `PhpStorm` and included by default in other JetBrains IDEs
3. `Karma`: For running Karma tests and coverage reporting

## Automatic testing

There two types of automates tests within this application:
1. `Unit tests`. Located next to each angular class (component/service/pipe, etc) with `.spec.ts` suffix. These tests are used to check the edge-cases of application logic and templates.
1. `Integration tests`. Located within `e2e` folder. Used to test application business requirements from the user's perspective. They run in browser against a certain URL.

`Unit tests` are your friends, whenever you break a piece of functionality, they will immediately let you know. Use them for TDD as well. It's OK to have many unit tests.

`Integration tests` are quite fragile and difficult to maintain, but they cover application functionality as a whole, including integration with external APIs. Run them on staging and on production, to make sure that new deployment works well (as much as this can be tested automatically).

## Continuous integration
Deployment works through Gitlab pipelines. Any member of the project can deploy to any environment.

There are two stages of pipelines:
1. `Test`. Unit-test are ran within development environment. PhantomJS browser is used.
1. `Deploy to enviroment`. Application is built for the chosen environment and then deployed to appropriate server.

Check details in `.gitlab-ci.yml`

## Angular2 notes

* For the purposes of this app all services are declared within `ServicesModule` and imported within the top possible level of DI hierarchy (`AppModule`), thus providing all services as singletons.
  
  Such a strategy keeps all services in memory, thus guarantees that you have the same instance of the service anywhere within the app. Performance regression is minor.
  
  Shall you need your own instance of the service, just enlist it within the component's `providers` section 

* There is a `SharedModule` which consists all of frequently used components and directives, like date/price pipe, paginator component, etc

* All entities definitions reside within `_entities` directory. Better avoid TS interfaces and stick to normal classes. Feel free to extend the entities, and group similar ones within the subdirectories.

## Notes for QA

* For quick manual testing you can spin up a local server with `yarn start`.
* Testing of big features should be performed on QA server, since it is almost identical to the production server.
* Feel free to deploy any commit to QA server anytime. This server is not used by developers nor users. 
