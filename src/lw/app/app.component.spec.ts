import { TestBed, async } from '@angular/core/testing';

import { AppComponent } from './app.component';
import {Component} from "@angular/core";
import {RouterLinkStubDirective, RouterOutletStubComponent} from "../../../testing/router-stubs";

@Component({selector: 'breadcrumbs', template: ''})
class BreadcrumbsStubComponent {}

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent, BreadcrumbsStubComponent,
        RouterLinkStubDirective, RouterOutletStubComponent,
      ],
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'LW'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('LW');
  }));
});
