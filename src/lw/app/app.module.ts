import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import '../_imports/rxjs-operators';

import { AppComponent } from './app.component';
import {AppRoutingModule} from "./app-routing.module";
import {ServicesModule} from "../_services/services.module";
import {SharedModule} from "../shared/shared.module";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ServicesModule,
    SharedModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
