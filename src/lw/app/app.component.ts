import { Component } from '@angular/core';
import {environment} from "../_environments/environment";

@Component({
  selector: 'lw-root',
  template: `
    <div class="container">
      <div class="row">
        <div class="col s12">
          <breadcrumbs></breadcrumbs>
    
          <router-outlet></router-outlet>
        </div>
      </div>
      <div class="row">
        <div class="col s12 right-align">
          Built for <b>{{env.envTitle}}</b> environment
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'LW';
  env = environment;
}
