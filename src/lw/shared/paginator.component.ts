import {Component, Input, Output, EventEmitter} from "@angular/core";
import {Paginator} from "../_entities/paginator";

/**
 * Usage
 * <paginator
 *    [paginator]="Paginator<ANY_ENTITY>"
 *    [showSummary]="boolean"
 *    (onSetPage)="setPage($event)"
 *    (onSetItemsPerPage)="setItemsPerPage($event)
 * ></paginator>
 */
@Component({
  selector: 'paginator',
  styleUrls: ['paginator.component.scss'],
  template: `
    <div class="pagination" *ngIf="_paginator?.total">
      <div class="row">
        <div class="col s2 results-count">
          <div *ngIf="showSummary" class="show-summary">
            <div>Items</div>
            <div>{{_paginator.from}} &mdash; {{_paginator.to}} of {{_paginator.total}}</div>
          </div>
        </div>
        <div class="col s8">
          <ul class="pages">
            <li *ngIf="_paginator.last_page > maxPages" [class.disabled]="_paginator.current_page === 1"
                class="waves-effect page-first">
              <a (click)="toFirstPage($event)" class="icon" title="First page"><i
                class="material-icons">first_page</i></a>
            </li>
            <li [class.disabled]="_paginator.current_page === 1"
                class="waves-effect page-prev">
              <a (click)="toPrevPage($event)" class="icon" title="Previous page"><i
                class="material-icons">chevron_left</i></a>
            </li>
            <li *ngFor="let page of pages" [class.active]="page === _paginator.current_page"
                class="waves-effect page-num">
              <a (click)="setPage($event, page)">{{page}}</a>
            </li>
            <li [class.disabled]="_paginator.current_page === _paginator.last_page"
                class="waves-effect page-next">
              <a (click)="toNextPage($event)" class="icon" title="Next page"><i
                class="material-icons">chevron_right</i></a>
            </li>
            <li *ngIf="_paginator.last_page > maxPages"
                [class.disabled]="_paginator.current_page === _paginator.last_page"
                class="waves-effect page-last">
              <a (click)="toLastPage($event)" class="icon" [title]="'Last page (' + _paginator.last_page + ')'"><i
                class="material-icons">last_page</i></a>
            </li>
          </ul>
        </div>
        <div class="input-field col s1 perpage-label">
          Per page:
        </div>
        <div class="input-field col s1 pages-dd">
          <select materialize="material_select" (change)="setItemsPerPage($event.target.value)">
            <option *ngFor="let value of itemsPerPageRange" [selected]="value == _paginator.per_page"
            >{{value}}</option>
          </select>
        </div>
      </div>
    </div>
  `
})
export class PaginatorComponent<T> {
  // Maximum number of pages to display
  maxPages: number = 9;

  pages: number[];

  itemsPerPageRange: number[] = [
    25, 50, 100, 200
  ];

  _paginator: Paginator<T>;

  @Output() onSetPage = new EventEmitter<number>();
  @Output() onSetItemsPerPage = new EventEmitter<number>();

  @Input()
  set paginator(paginator: Paginator<T>) {
    this._paginator = paginator;
    this.pages = this.genPager(paginator);
  }

  @Input() showSummary: boolean = true;

  constructor() {}

  genPager(paginator: Paginator<T>): number[] {
    let pages: number[] = [];

    // When pages range >= maxPages, keep the selected page in the middle
    let middlePosition: number = Math.ceil(this.maxPages / 2);
    // First and last pages in range to display
    let firstPage: number;
    let lastPage: number;

    if (paginator.current_page <= middlePosition || paginator.last_page <= this.maxPages) { // Beginning of the range
      firstPage = 1;
      lastPage = paginator.last_page < this.maxPages ? paginator.last_page : this.maxPages;
    } else if (paginator.current_page > paginator.last_page - middlePosition) { // end of the range
      firstPage = paginator.last_page - this.maxPages + 1;
      lastPage = paginator.last_page;
    } else { // middle of the range
      firstPage = paginator.current_page - middlePosition + 1;
      lastPage = firstPage + this.maxPages - 1;
    }

    for (let i = firstPage; i <= lastPage; i++) {
      pages.push(i);
    }

    return pages;
  }

  private _setPage(page: number) {
    this.onSetPage.emit(page);
  }

  setPage($e: MouseEvent, page: number) {
    this._setPage(page);
  }

  setItemsPerPage(itemsPerPage: string) {
    this.onSetItemsPerPage.emit(parseInt(itemsPerPage));
  }

  toNextPage($e: MouseEvent) {
    if (this._paginator.current_page < this._paginator.last_page) {
      this._setPage(this._paginator.current_page + 1);
    }
  }

  toPrevPage($e: MouseEvent) {
    if (this._paginator.current_page > 1) {
      this._setPage(this._paginator.current_page - 1);
    }
  }

  toFirstPage($e: MouseEvent) {
    if (this._paginator.current_page != 1) {
      this._setPage(1);
    }
  }

  toLastPage($e: MouseEvent) {
    if (this._paginator.current_page != this._paginator.last_page) {
      this._setPage(this._paginator.last_page);
    }
  }
}
