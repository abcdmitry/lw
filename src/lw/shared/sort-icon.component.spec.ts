import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {SortIconComponent} from "./sort-icon.component";
import {Component, DebugElement} from "@angular/core";
import {By} from "@angular/platform-browser";

@Component({
  template: `<sort-icon [field]="field" [sortedBy]="sortedBy"></sort-icon>`
})
class TestHostComponent {
  field: string;
  sortedBy: string;
}

describe('SortIcon component', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SortIconComponent, TestHostComponent],
    }).compileComponents();
  }));

  describe('With host component', () => {
    let fixture: ComponentFixture<TestHostComponent>;
    let component: TestHostComponent;
    let debugElement: DebugElement;

    beforeEach(() => {
      fixture = TestBed.createComponent(TestHostComponent);
      component = fixture.componentInstance;
      debugElement = fixture.debugElement;
    });

    it('should create the component', () => {
      expect(component).toBeTruthy();
    });

    it('should calculate direction correctly', () => {
      let testData = [
        {testInput: {field: 'fieldName1', sortedBy: 'fieldName1'}, expected: 'arrow_downward [A-Z]'},
        {testInput: {field: 'fieldName1', sortedBy: '-fieldName1'}, expected: 'arrow_upward [Z-A]'},
        {testInput: {field: 'f', sortedBy: 'f'}, expected: 'arrow_downward [A-Z]'},
        {testInput: {field: 'f', sortedBy: '-f'}, expected: 'arrow_upward [Z-A]'},
      ];

      testData.forEach(value => {
        component.field = value.testInput.field;
        component.sortedBy = value.testInput.sortedBy;
        fixture.detectChanges();

        let el = debugElement.query(By.css('span span'));

        expect(el.nativeElement.textContent.trim()).toBe(value.expected);
      });
    });

    it('should not show sort icon when sorted by another field', () => {
      let testData = [
        {testInput: {field: 'fieldName1', sortedBy: 'fieldName2'}},
      ];

      testData.forEach(value => {
        component.field = value.testInput.field;
        component.sortedBy = value.testInput.sortedBy;
        fixture.detectChanges();

        let el = debugElement.query(By.css('span span'));

        expect(el).toBeNull();
      });
    });
  });
});
