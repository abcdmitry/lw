import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {PaginatorComponent} from "./paginator.component";
import {Paginator} from "../_entities/paginator";
import {Component, DebugElement} from "@angular/core";
import {By} from "@angular/platform-browser";
import {click} from "../../../testing/index";

/**
 * This spec is intended to test Paginator component.
 * There are two test suites:
 * 1. <paginator> with input properties injected into the host component.
 *    So paginator component is initialized with angular framework, we don't care how it is done.
 *    We test:
 *    a) Template rendering with provided data
 *    b) Calling host component methods triggered by events from paginator component
 * 2. Paginator component tested stand-alone. Only logic of the component methods is tested here
 *    with various edge-cases
 */

class TestEntity {
  id: string;
  name: string;
}

@Component({
  template: `<paginator 
    [showSummary]="showSummary" 
    [paginator]="paginator"
    (onSetPage)="setPage($event)"
    (onSetItemsPerPage)="setItemsPerPage($event)"
  ></paginator>`
})
class TestHostComponent {
  paginator: Paginator<TestEntity>;
  showSummary: boolean = true;

  page: number;
  itemsPerPage: number;

  setPage($event: number) {
    this.page = $event;
  }

  setItemsPerPage($event: number) {
    this.itemsPerPage = $event;
  }
}

/**
 * Generate a paginator object emulating backend response
 */
function generatePaginatorStub(total: number, currentPage: number, perPage: number): Paginator<TestEntity> {
  let items: TestEntity[] = [];

  for (let i = (currentPage - 1) * perPage + 1; i <= currentPage * perPage; i++) {
    items.push({id: i.toString(), name: `Item ${i}`});
  }

  let expectedMax = currentPage * perPage;
  let paginated: Paginator<TestEntity> = new Paginator<TestEntity>(items);
  paginated.total = total;
  paginated.per_page = perPage;
  paginated.current_page = currentPage;
  paginated.last_page = Math.ceil(paginated.total / paginated.per_page) || 0;
  paginated.from = (currentPage - 1) * perPage + 1;
  paginated.to = expectedMax > paginated.total ? paginated.total : expectedMax;

  return paginated;
}

describe('PaginatorComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PaginatorComponent, TestHostComponent],
    }).compileComponents();
  }));

  describe('With host component', () => {
    let fixture: ComponentFixture<TestHostComponent>;
    let component: TestHostComponent;
    let debugElement: DebugElement;

    beforeEach(() => {
      fixture = TestBed.createComponent(TestHostComponent);
      component = fixture.componentInstance;
      debugElement = fixture.debugElement;
    });

    it('should create the component', () => {
      expect(component).toBeTruthy();
    });

    it('should not render paginator when there are no items', () => {
      component.paginator = generatePaginatorStub(0, 0, 0);
      fixture.detectChanges();
      let el = debugElement.query(By.css('.pagination'));
      expect(el).toBeNull();
    });

    it('should render exact number of pages when number of pages < 9, with correct page numbers', () => {
      component.paginator = generatePaginatorStub(65, 3, 10); // 7 pages
      fixture.detectChanges();
      let pagesEl = debugElement.queryAll(By.css('ul.pages li.page-num a'));
      expect(pagesEl.length).toEqual(7);

      for(let i = 1; i <= 7; i++) {
        expect(pagesEl[i - 1].nativeElement.textContent).toBe(i.toString());
      }
    });

    it('should render 9 pages when number of pages = 9, with correct page numbers', () => {
      component.paginator = generatePaginatorStub(85, 3, 10); // 9 pages
      fixture.detectChanges();
      let pagesEl = debugElement.queryAll(By.css('ul.pages li.page-num a'));
      expect(pagesEl.length).toEqual(9);

      for(let i = 1; i <= 9; i++) {
        expect(pagesEl[i - 1].nativeElement.textContent).toBe(i.toString());
      }
    });

    it('should render 9 pages when number of pages > 9, with correct page numbers', () => {
      component.paginator = generatePaginatorStub(115, 11, 5);
      fixture.detectChanges();
      let pagesEl = debugElement.queryAll(By.css('ul.pages li.page-num a'));
      expect(pagesEl.length).toEqual(9);

      for(let i = 7; i <= 15; i++) {
        expect(pagesEl[i - 7].nativeElement.textContent).toBe(i.toString());
      }
    });

    it('should always render Prev/Next buttons', () => {
      let inputProvider = [
        //[total, currentPage, perPage]
        [5, 1, 5], // 1 page
        [4, 1, 5], // 1 page
        [27, 3, 5], // 6 pages
        [27, 6, 5], // 6 pages
        [61, 13, 5] // 13 pages
      ];

      inputProvider.forEach(inputSet => {
        component.paginator = generatePaginatorStub(inputSet[0], inputSet[1], inputSet[2]);
        fixture.detectChanges();
        let elPrevNext = debugElement.queryAll(By.css('ul.pages li.page-prev, ul.pages li.page-next'));
        expect(elPrevNext.length).toEqual(2);
      });
    });

    it('should not render First/Last buttons when number of pages <= 9', () => {
      let inputProvider = [
        //[total, currentPage, perPage]
        [27, 3, 5], // 6 pages
        [45, 9, 5], // 9 pages
      ];

      inputProvider.forEach(inputSet => {
        component.paginator = generatePaginatorStub(inputSet[0], inputSet[1], inputSet[2]);
        fixture.detectChanges();
        let elFirstLast = debugElement.queryAll(By.css('ul.pages li.page-first, ul.pages li.page-last'));
        expect(elFirstLast.length).toEqual(0);
      });
    });

    it('should render First/Last buttons when number of pages > 9', () => {
      component.paginator = generatePaginatorStub(50, 2, 5);
      fixture.detectChanges();
      let elFirstLast = debugElement.queryAll(By.css('ul.pages li.page-first, ul.pages li.page-last'));
      expect(elFirstLast.length).toEqual(2);
    });

    it('should disable Prev page button when on the first page', () => {
      component.paginator = generatePaginatorStub(10, 1, 5);
      fixture.detectChanges();
      let el = debugElement.query(By.css('ul.pages li.page-prev'));
      expect(el.nativeElement.classList).toContain('disabled');
    });

    it('should not disable Prev page button when not on the first page', () => {
      component.paginator = generatePaginatorStub(10, 2, 5);
      fixture.detectChanges();
      let el = debugElement.query(By.css('ul.pages li.page-prev'));
      expect(el.nativeElement.classList).not.toContain('disabled');
    });

    it('should disable Next page button when on the last page', () => {
      let inputProvider = [
        //[total, currentPage, perPage]
        [1, 1, 1], // 1 page
        [27, 6, 5], // 6 pages
        [45, 9, 5], // 9 pages
      ];

      inputProvider.forEach(inputSet => {
        component.paginator = generatePaginatorStub(inputSet[0], inputSet[1], inputSet[2]);
        fixture.detectChanges();
        let el = debugElement.query(By.css('ul.pages li.page-next'));
        expect(el.nativeElement.classList).toContain('disabled');
      });
    });

    it('should not disable Next page button when not on the last page', () => {
      component.paginator = generatePaginatorStub(10, 1, 5);
      fixture.detectChanges();
      let el = debugElement.query(By.css('ul.pages li.page-next'));
      expect(el.nativeElement.classList).not.toContain('disabled');
    });

    it('should disable First page button when on the first page', () => {
      component.paginator = generatePaginatorStub(50, 1, 5);
      fixture.detectChanges();
      let el = debugElement.query(By.css('ul.pages li.page-first'));
      expect(el.nativeElement.classList).toContain('disabled');
    });

    it('should not disable First page button when not on the first page', () => {
      let inputProvider = [
        //[total, currentPage, perPage]
        [50, 2, 5], // 10 pages
        [100, 19, 5], // 20 pages
      ];

      inputProvider.forEach(inputSet => {
        component.paginator = generatePaginatorStub(inputSet[0], inputSet[1], inputSet[2]);
        fixture.detectChanges();
        let el = debugElement.query(By.css('ul.pages li.page-first'));
        expect(el.nativeElement.classList).not.toContain('disabled');
      });
    });

    it('should disable Last page button when on the last page', () => {
      component.paginator = generatePaginatorStub(50, 10, 5);
      fixture.detectChanges();
      let el = debugElement.query(By.css('ul.pages li.page-last'));
      expect(el.nativeElement.classList).toContain('disabled');
    });

    it('should not disable Last page button when not on the last page', () => {
      let inputProvider = [
        //[total, currentPage, perPage]
        [50, 1, 5], // 10 pages
        [50, 2, 5], // 10 pages
        [100, 19, 5], // 20 pages
      ];

      inputProvider.forEach(inputSet => {
        component.paginator = generatePaginatorStub(inputSet[0], inputSet[1], inputSet[2]);
        fixture.detectChanges();
        let el = debugElement.query(By.css('ul.pages li.page-last'));
        expect(el.nativeElement.classList).not.toContain('disabled');
      });
    });

    it('should render summary block when showSummary input is true', () => {
      component.paginator = generatePaginatorStub(1, 1, 1);
      component.showSummary = true;
      fixture.detectChanges();
      let el = debugElement.query(By.css('.results-count .show-summary'));
      expect(el).toBeTruthy();
    });

    it('should not render summary block when showSummary input is false', () => {
      component.paginator = generatePaginatorStub(1, 1, 1);
      component.showSummary = false;
      fixture.detectChanges();
      let el = debugElement.query(By.css('.results-count .show-summary'));
      expect(el).toBeFalsy();
    });

    it('should emit an event when First page button is active and was clicked', () => {
      component.paginator = generatePaginatorStub(100, 17, 5);
      fixture.detectChanges();
      let el = debugElement.query(By.css('ul.pages li.page-first'));
      expect(el.nativeElement.classList).not.toContain('disabled');

      el = el.query(By.css('a'));

      click(el);

      expect(component.page).toEqual(1);
    });

    it('should not emit an event when First page button is not active and was clicked', () => {
      component.paginator = generatePaginatorStub(100, 1, 5);
      fixture.detectChanges();
      let el = debugElement.query(By.css('ul.pages li.page-first'));
      expect(el.nativeElement.classList).toContain('disabled');

      el = el.query(By.css('a'));

      click(el);

      expect(component.page).not.toBeDefined();
    });

    it('should emit an event when Last page button is active and was clicked', () => {
      component.paginator = generatePaginatorStub(100, 19, 5); // 20 pages
      fixture.detectChanges();
      let el = debugElement.query(By.css('ul.pages li.page-last'));
      expect(el.nativeElement.classList).not.toContain('disabled');

      el = el.query(By.css('a'));

      click(el);

      expect(component.page).toEqual(20);
    });

    it('should not emit an event when Last page button is not active and was clicked', () => {
      component.paginator = generatePaginatorStub(100, 20, 5); // 20 pages
      fixture.detectChanges();
      let el = debugElement.query(By.css('ul.pages li.page-last'));
      expect(el.nativeElement.classList).toContain('disabled');

      el = el.query(By.css('a'));

      click(el);

      expect(component.page).not.toBeDefined();
    });

    it('should emit an event when Prev page button is active and was clicked', () => {
      let inputProvider = [
        [15, 3, 5], // 3 pages
        [15, 2, 5], // 3 pages
      ];

      inputProvider.forEach(inputSet => {
        component.paginator = generatePaginatorStub(inputSet[0], inputSet[1], inputSet[2]);
        fixture.detectChanges();
        let el = debugElement.query(By.css('ul.pages li.page-prev'));
        expect(el.nativeElement.classList).not.toContain('disabled');

        el = el.query(By.css('a'));

        click(el);

        expect(component.page).toEqual(inputSet[1] - 1);
      });
    });

    it('should not emit an event when Prev page button is not active and was clicked', () => {
      component.paginator = generatePaginatorStub(100, 1, 5);
      fixture.detectChanges();
      let el = debugElement.query(By.css('ul.pages li.page-prev'));
      expect(el.nativeElement.classList).toContain('disabled');

      el = el.query(By.css('a'));

      click(el);

      expect(component.page).not.toBeDefined();
    });

    it('should emit an event when Next page button is active and was clicked', () => {
      let inputProvider = [
        [15, 1, 5], // 3 pages
        [15, 2, 5], // 3 pages
      ];

      inputProvider.forEach(inputSet => {
        component.paginator = generatePaginatorStub(inputSet[0], inputSet[1], inputSet[2]);
        fixture.detectChanges();
        let el = debugElement.query(By.css('ul.pages li.page-next'));
        expect(el.nativeElement.classList).not.toContain('disabled');

        el = el.query(By.css('a'));

        click(el);

        expect(component.page).toEqual(inputSet[1] + 1);
      });
    });

    it('should not emit an event when Next page button is not active and was clicked', () => {
      component.paginator = generatePaginatorStub(10, 2, 5);
      fixture.detectChanges();
      let el = debugElement.query(By.css('ul.pages li.page-next'));
      expect(el.nativeElement.classList).toContain('disabled');

      el = el.query(By.css('a'));

      click(el);

      expect(component.page).not.toBeDefined();
    });

    it('should emit an event when switching pages manually', () => {
      component.paginator = generatePaginatorStub(45, 2, 5);
      fixture.detectChanges();
      let el = debugElement.queryAll(By.css('ul.pages li.page-num a'));

      for (let i = 0; i < el.length; i++) {
        click(el[i]);
        expect(component.page).toEqual(i + 1);
      }
    });

    it('should emit an event when items per page value has been changed', () => {
      component.paginator = generatePaginatorStub(45, 3, 5);
      fixture.detectChanges();
      let el = debugElement.query(By.css('.pages-dd select'));

      // el.nativeElement.selectedIndex = 3;
      el.triggerEventHandler('change', {target: {value: 200}});

      expect(component.itemsPerPage).toEqual(200);
    });
  });

  describe('Isolated', () => {
    it('should generate pages range correctly', () => {
      let dataProvider = [
        {maxPages: 4, current_page: 1, last_page: 3, expectedRange: {from: 1, to: 3}},
        {maxPages: 4, current_page: 2, last_page: 3, expectedRange: {from: 1, to: 3}},
        {maxPages: 4, current_page: 3, last_page: 3, expectedRange: {from: 1, to: 3}},

        {maxPages: 4, current_page: 1, last_page: 4, expectedRange: {from: 1, to: 4}},
        {maxPages: 4, current_page: 2, last_page: 4, expectedRange: {from: 1, to: 4}},
        {maxPages: 4, current_page: 3, last_page: 4, expectedRange: {from: 1, to: 4}},
        {maxPages: 4, current_page: 4, last_page: 4, expectedRange: {from: 1, to: 4}},

        {maxPages: 4, current_page: 1, last_page: 5, expectedRange: {from: 1, to: 4}},
        {maxPages: 4, current_page: 2, last_page: 5, expectedRange: {from: 1, to: 4}},
        {maxPages: 4, current_page: 3, last_page: 5, expectedRange: {from: 2, to: 5}},
        {maxPages: 4, current_page: 4, last_page: 5, expectedRange: {from: 2, to: 5}},
        {maxPages: 4, current_page: 5, last_page: 5, expectedRange: {from: 2, to: 5}},

        {maxPages: 4, current_page: 1, last_page: 6, expectedRange: {from: 1, to: 4}},
        {maxPages: 4, current_page: 2, last_page: 6, expectedRange: {from: 1, to: 4}},
        {maxPages: 4, current_page: 3, last_page: 6, expectedRange: {from: 2, to: 5}},
        {maxPages: 4, current_page: 4, last_page: 6, expectedRange: {from: 3, to: 6}},
        {maxPages: 4, current_page: 5, last_page: 6, expectedRange: {from: 3, to: 6}},
        {maxPages: 4, current_page: 6, last_page: 6, expectedRange: {from: 3, to: 6}},

        {maxPages: 4, current_page: 1, last_page: 7, expectedRange: {from: 1, to: 4}},
        {maxPages: 4, current_page: 2, last_page: 7, expectedRange: {from: 1, to: 4}},
        {maxPages: 4, current_page: 3, last_page: 7, expectedRange: {from: 2, to: 5}},
        {maxPages: 4, current_page: 4, last_page: 7, expectedRange: {from: 3, to: 6}},
        {maxPages: 4, current_page: 5, last_page: 7, expectedRange: {from: 4, to: 7}},
        {maxPages: 4, current_page: 6, last_page: 7, expectedRange: {from: 4, to: 7}},
        {maxPages: 4, current_page: 7, last_page: 7, expectedRange: {from: 4, to: 7}},

        {maxPages: 5, current_page: 1, last_page: 4, expectedRange: {from: 1, to: 4}},
        {maxPages: 5, current_page: 2, last_page: 4, expectedRange: {from: 1, to: 4}},
        {maxPages: 5, current_page: 3, last_page: 4, expectedRange: {from: 1, to: 4}},
        {maxPages: 5, current_page: 4, last_page: 4, expectedRange: {from: 1, to: 4}},

        {maxPages: 5, current_page: 1, last_page: 5, expectedRange: {from: 1, to: 5}},
        {maxPages: 5, current_page: 2, last_page: 5, expectedRange: {from: 1, to: 5}},
        {maxPages: 5, current_page: 3, last_page: 5, expectedRange: {from: 1, to: 5}},
        {maxPages: 5, current_page: 4, last_page: 5, expectedRange: {from: 1, to: 5}},
        {maxPages: 5, current_page: 5, last_page: 5, expectedRange: {from: 1, to: 5}},

        {maxPages: 5, current_page: 1, last_page: 6, expectedRange: {from: 1, to: 5}},
        {maxPages: 5, current_page: 2, last_page: 6, expectedRange: {from: 1, to: 5}},
        {maxPages: 5, current_page: 3, last_page: 6, expectedRange: {from: 1, to: 5}},
        {maxPages: 5, current_page: 4, last_page: 6, expectedRange: {from: 2, to: 6}},
        {maxPages: 5, current_page: 5, last_page: 6, expectedRange: {from: 2, to: 6}},
        {maxPages: 5, current_page: 6, last_page: 6, expectedRange: {from: 2, to: 6}},

        {maxPages: 5, current_page: 1, last_page: 7, expectedRange: {from: 1, to: 5}},
        {maxPages: 5, current_page: 2, last_page: 7, expectedRange: {from: 1, to: 5}},
        {maxPages: 5, current_page: 3, last_page: 7, expectedRange: {from: 1, to: 5}},
        {maxPages: 5, current_page: 4, last_page: 7, expectedRange: {from: 2, to: 6}},
        {maxPages: 5, current_page: 5, last_page: 7, expectedRange: {from: 3, to: 7}},
        {maxPages: 5, current_page: 6, last_page: 7, expectedRange: {from: 3, to: 7}},
        {maxPages: 5, current_page: 7, last_page: 7, expectedRange: {from: 3, to: 7}},

        {maxPages: 5, current_page: 1, last_page: 8, expectedRange: {from: 1, to: 5}},
        {maxPages: 5, current_page: 2, last_page: 8, expectedRange: {from: 1, to: 5}},
        {maxPages: 5, current_page: 3, last_page: 8, expectedRange: {from: 1, to: 5}},
        {maxPages: 5, current_page: 4, last_page: 8, expectedRange: {from: 2, to: 6}},
        {maxPages: 5, current_page: 5, last_page: 8, expectedRange: {from: 3, to: 7}},
        {maxPages: 5, current_page: 6, last_page: 8, expectedRange: {from: 4, to: 8}},
        {maxPages: 5, current_page: 7, last_page: 8, expectedRange: {from: 4, to: 8}},
        {maxPages: 5, current_page: 8, last_page: 8, expectedRange: {from: 4, to: 8}},
      ];
      let component = new PaginatorComponent();

      dataProvider.forEach(dataSet => {
        component.maxPages = dataSet.maxPages;
        let result: number[] = component.genPager(<Paginator<TestEntity>>{
          current_page: dataSet.current_page,
          last_page: dataSet.last_page,
        });

        let expectedResult: number[] = [];
        for (let i = dataSet.expectedRange.from; i <= dataSet.expectedRange.to; i++) {
          expectedResult.push(i);
        }

        expect(result).toEqual(expectedResult);
      });
    });
  });
});
