import {Breadcrumb} from "../_entities/breadcrumb";
import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {BreadcrumbsComponent} from "./breadcrumbs.component";
import {DebugElement, Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {NavService} from "../_services/nav.service";
import {By} from "@angular/platform-browser";
import {RouterLinkStubDirective} from "../../../testing/router-stubs";

const breadcrumbsTestData: Breadcrumb[] = [
  {path: ['/url1'], fullUrl: '/url1', title: 'Title 1'},
  {path: ['/url1/url2'], fullUrl: '/url1/url2', title: 'Title 2'},
  {path: ['/url1/url2/url3'], fullUrl: '/url1/url2/url3', title: 'Title 3'},
];

@Injectable()
class NavServiceStub {
  getBreadcrumbs$(): Observable<Breadcrumb[]> {
    return Observable.of(breadcrumbsTestData);
  }
}

describe('BreadcrumbsComponent', () => {
  let component: BreadcrumbsComponent;
  let fixture: ComponentFixture<BreadcrumbsComponent>;
  let debugElement: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BreadcrumbsComponent, RouterLinkStubDirective],
      providers: [{provide: NavService, useClass: NavServiceStub}],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreadcrumbsComponent);
    debugElement = fixture.debugElement;
    component = fixture.componentInstance;
  });

  it('should create component', () => {
    expect(component).toBeDefined();
  });

  it('should render breadcrumbs', () => {
    fixture.detectChanges();

    let items = debugElement.queryAll(By.css('div.breadcrumbs a.breadcrumb'));

    for (let i = 0; i < items.length; i++) {
      expect(items[i].nativeElement.textContent).toEqual(breadcrumbsTestData[i].title);
    }
  });
});
