import {Component, OnInit} from "@angular/core";
import {NavService} from "../_services/nav.service";
import {Observable} from "rxjs/Observable";
import {Breadcrumb} from "../_entities/breadcrumb";

@Component({
  selector: 'breadcrumbs',
  template: `
    <nav>
      <div class="nav-wrapper breadcrumbs">
        <div class="col s12">
          <a 
            *ngFor="let breadcrumb of breadcrumbs$ | async; let l = last" 
            [routerLink]="[breadcrumb.fullUrl]"
            class="breadcrumb"
          >{{breadcrumb.title}}</a>
        </div>
      </div>
    </nav>
  `
})
export class BreadcrumbsComponent implements OnInit {
  breadcrumbs$: Observable<Breadcrumb[]>;

  constructor(private navService: NavService) {}

  ngOnInit() {
    this.breadcrumbs$ = this.navService.getBreadcrumbs$();
  }
}
