import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {BreadcrumbsComponent} from "./breadcrumbs.component";
import {RouterModule} from "@angular/router";
import {PaginatorComponent} from "./paginator.component";
import {MaterializeModule} from "angular2-materialize";
import {SortIconComponent} from "./sort-icon.component";

/**
 * This is a feature module consisting of all most frequently used components.
 *
 * Do not add lots of stuff here. Just really common things like date pipe, price pipe, paginator component, etc
 */
@NgModule({
  imports: [CommonModule, MaterializeModule, RouterModule],
  declarations: [
    BreadcrumbsComponent,
    PaginatorComponent,
    SortIconComponent,
  ],
  exports: [
    CommonModule,
    MaterializeModule,
    BreadcrumbsComponent,
    PaginatorComponent,
    SortIconComponent,
  ],
})
export class SharedModule {}
