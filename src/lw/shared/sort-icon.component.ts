import {Component, Input} from "@angular/core";

@Component({
  selector: 'sort-icon',
  template: `
    <span *ngIf="direction" class="sorting-widget light-blue-text text-darken-4">
      <span *ngIf="direction == '+'"><i class="material-icons">arrow_downward</i> [A-Z] </span>
      <span *ngIf="direction == '-'"><i class="material-icons">arrow_upward</i> [Z-A] </span>
    </span>
  `
})
export class SortIconComponent {
  @Input() field: string;
  @Input() sortedBy: string;

  get direction(): string | null {
    if (this.field == this.sortedBy) {
      return '+';
    } else if ('-' + this.field == this.sortedBy) {
      return '-';
    } else {
      return null;
    }
  }
}
