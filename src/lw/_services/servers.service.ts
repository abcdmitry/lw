import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {Server} from "../_entities/server";
import {Paginator} from "../_entities/paginator";
import {FakeBackendService} from "./fake-backend.service";
import {ServersListFilter} from "../_entities/servers/servers-list-filter";
import {ServersListFiltersDict} from "../_entities/servers/servers-list-filters-dict";

@Injectable()
export class ServersService {
  constructor(private fakeBackend: FakeBackendService) {}

  getServers$(
    page: number = 1,
    perPage: number = 25,
    filter: ServersListFilter = {},
    sort: string = 'model'
  ): Observable<Paginator<Server>> {
    return this.fakeBackend.getServers$(page, perPage, filter, sort);
  }

  getServer$(id: number): Observable<Server> {
    return this.fakeBackend.getServer$(id);
  }

  getFiltersDict$(): Observable<ServersListFiltersDict> {
    return this.fakeBackend.getFiltersDict$();
  }
}
