import {Title} from "@angular/platform-browser";
import {inject, TestBed} from "@angular/core/testing";
import {NavService} from "./nav.service";
import {Breadcrumb} from "../_entities/breadcrumb";
import {ActivatedRoute, Router} from "@angular/router";
import {ActivatedRouteStub, RouterStub} from "../../../testing/router-stubs";

const breadcrumbsTestData: Breadcrumb[] = [
  {path: ['/url1'], fullUrl: undefined, title: 'Title 1'},
  {path: ['/url1/url2'], fullUrl: undefined, title: 'Title 2'},
  {path: ['/url1/url2/url3'], fullUrl: undefined, title: 'Title 3'}
];

describe('NavService', () => {
  let navService: NavService;
  let titleServiceSpy: jasmine.Spy;
  let routerSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Title, NavService,
        {provide: Router, useClass: RouterStub},
        {provide: ActivatedRoute, useClass: ActivatedRouteStub}
      ]
    });

    navService = TestBed.get(NavService);
    titleServiceSpy = spyOn(TestBed.get(Title), 'setTitle');
    routerSpy = spyOn(TestBed.get(Router), 'createUrlTree').and.callThrough();
  });

  it('should create the service', () => {
    expect(navService).toBeTruthy();
  });

  it('should emit new breadcrumbs with a link to the Main page in the beginning',
    inject([ActivatedRoute], (route: ActivatedRoute) => {

    navService.setNav(breadcrumbsTestData, route);

    navService.getBreadcrumbs$().subscribe(breadcrumbs => {
      expect(breadcrumbs[0].path).toEqual(['/']);
      expect(breadcrumbs.slice(1)).toEqual(breadcrumbsTestData);
    });
  }));

  it('should set title to the last item of breadcrumbs chain',
    inject([ActivatedRoute], (route: ActivatedRoute) => {
    navService.setNav(breadcrumbsTestData, route);

    expect(titleServiceSpy).toHaveBeenCalledWith(breadcrumbsTestData.slice(-1)[0].title);
  }));
});
