import {NgModule} from "@angular/core";
import {NavService} from "./nav.service";
import {ServersService} from "./servers.service";
import {HttpModule} from "@angular/http";
import {FakeBackendService} from "./fake-backend.service";

@NgModule({
  imports: [HttpModule],
  providers: [NavService, FakeBackendService, ServersService],
})
export class ServicesModule {}
