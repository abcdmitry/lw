import {Injectable} from "@angular/core";
import {Title} from "@angular/platform-browser";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Breadcrumb} from "../_entities/breadcrumb";
import {Observable} from "rxjs/Observable";
import {ActivatedRoute, Router} from "@angular/router";

@Injectable()
export class NavService {
  private breadcrumbsSubject: BehaviorSubject<Breadcrumb[]>;
  private breadcrumbs$: Observable<Breadcrumb[]>;

  constructor(
    private title: Title,
    private router: Router
  ) {
    this.breadcrumbsSubject = new BehaviorSubject([]);
    this.breadcrumbs$ = this.breadcrumbsSubject.asObservable();
  }

  private setBreadcrumbs(path: Breadcrumb[]): void {
    this.breadcrumbsSubject.next(path);
  }

  setNav(path: Breadcrumb[], route: ActivatedRoute): void {
    let newPath: Breadcrumb[] = path.map(v => Object.assign({}, v));
    newPath.unshift({path: ['/'], title: 'Main'});

    newPath = newPath.map(breadcrumb => {
      breadcrumb.fullUrl = this.router.createUrlTree(breadcrumb.path, {relativeTo: route}).toString();
      return breadcrumb;
    });

    this.setBreadcrumbs(newPath);

    this.title.setTitle(newPath.slice(-1)[0].title);
  }

  getBreadcrumbs$(): Observable<Breadcrumb[]> {
    return this.breadcrumbs$;
  }
}
