import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {ServersListFilter} from "../_entities/servers/servers-list-filter";
import {Paginator} from "../_entities/paginator";
import {Server} from "../_entities/server";
import {ServersListFiltersDict} from "../_entities/servers/servers-list-filters-dict";
import {ReplaySubject} from "rxjs/ReplaySubject";
import {Observable} from "rxjs/Observable";
import {BehaviorSubject} from "rxjs/BehaviorSubject";

/**
 * This is a backend simulation service.
 */
@Injectable()
export class FakeBackendService {
  private serversSubject: ReplaySubject<Server[]>;
  private filtersSubject: BehaviorSubject<ServersListFiltersDict>;

  private filtersDict: ServersListFiltersDict = {
    ramGb: [2, 4, 8,  12, 16, 24, 32, 48, 64, 96, 128],
    hddGb: [0, 250, 500, 1000, 2000, 3000, 4000, 8000, 12000, 24000, 48000, 72000],
    hddType: ['SAS', 'SATA', 'SSD'],
    location: [],
  };

  private eurRates: {[currency: string]: number} = {
    sgd: 1.607,
    usd: 1.1963,
  };

  constructor(private http: Http) {
    this.filtersSubject = new BehaviorSubject<ServersListFiltersDict>(this.filtersDict);
  }

  getFiltersDict$() {
    return this.filtersSubject.asObservable();
  }

  /**
   * Return cached response if available. Otherwise make an http request to /assets/servers.json
   * and populate data with auxiliary fields.
   *
   * Response is cached inside the serversSubject
   *
   * Calling with refresh = true makes a new http request
   */
  private fetchServersData(refresh: boolean = false): Observable<Server[]> {
    if (!this.serversSubject || refresh) {
      if (!this.serversSubject) { // In case it's a refresh request, we should not reinitialize the serversSubject
        this.serversSubject = new ReplaySubject<Server[]>(1);
      }
      this.http.get('/assets/servers.json')
        .map(value => value.json())
        .map(items => this.prepareExtendedData(items))
        // this.filtersDict.location has been changed as a side-effect of previous operator
        // Let's sort the locations and then emit new filtersDict
        .do(v => {
          this.filtersDict.location.sort();
          this.filtersSubject.next(this.filtersDict);
        })
        .subscribe(items => this.serversSubject.next(items));
    }
    return this.serversSubject.asObservable();
  }

  getServers$(
    page: number = 1,
    perPage: number = 25,
    filter: ServersListFilter,
    sort: string = 'model',
  ): Observable<Paginator<Server>> {
    return this.fetchServersData()
      .map(items => this.applyFilter(items, filter))
      .map(items => this.applySort(items, sort))
      .map(items => this.paginate<Server>(items, page, perPage));
  }

  getServer$(id: number): Observable<Server> {
    return this.fetchServersData()
      .map(items => items.filter(server => server.id == id).slice(0, 1)[0]);
  }

  /**
   * Calculate additional fields to improve search, filtering and sorting
   */
  prepareExtendedData(items: Server[]): Server[] {
    // Operations with filtersDict.location is a side-effect,
    // but let's keep it in order to avoid additional loop through all items
    this.filtersDict.location = [];

    return items.map(server => {
      server.searchString = this.normalizeSearchString(server.model);

      let hddInfo = server.hdd.match(/^(\d+)x(\d+)([G|T]B)(SAS|SATA2|SSD)/i);
      server.hddGb = (hddInfo[3] == 'TB' ? 1000 : 1) * parseInt(hddInfo[1]) * parseInt(hddInfo[2]);
      server.hddType = hddInfo[4] == 'SATA2' ? 'SATA' : hddInfo[4];

      server.ramGb = parseInt(server.ram.match(/^(\d+)GB/i)[1]);

      let currencyMatch = server.price.match(/^(€|\$|S\$)(\d+\.\d{2})/);
      if (currencyMatch[1] == '€') {
        server.priceEUR = parseFloat(currencyMatch[2]);
      } else if (currencyMatch[1] == '$') {
        server.priceEUR = parseFloat(currencyMatch[2]) / this.eurRates.usd;
      } else if (currencyMatch[1] == 'S$') {
        server.priceEUR = parseFloat(currencyMatch[2]) / this.eurRates.sgd;
      }
      server.priceEUR = Math.ceil(server.priceEUR * 100) / 100;
      server.currency = currencyMatch[1];

      server.location = server.location.trim();

      if (this.filtersDict.location.indexOf(server.location) === -1) {
        this.filtersDict.location.push(server.location);
      }

      return server;
    });
  }

  /**
   * Only leave letters, digits and underscores in the given string
   */
  normalizeSearchString(search: string) {
    return search.replace(/\W/g, '').toLocaleLowerCase();
  }

  applyFilter(items: Server[], filter: ServersListFilter): Server[] {
    return items.filter(server => {
      if (filter.hddType && server.hddType != filter.hddType) {
        return false;
      } else if (filter.ramGb && filter.ramGb.length > 0 && filter.ramGb.indexOf(server.ramGb) === -1) {
        return false;
      } else if (filter.free_search && server.searchString.indexOf(this.normalizeSearchString(filter.free_search)) === -1) {
        return false;
      } else if (filter.location && server.location != filter.location) {
        return false;
      } else if (filter.hddGb_from && filter.hddGb_from > this.filtersDict.hddGb[0] && server.hddGb < filter.hddGb_from) {
        return false;
      } else if (filter.hddGb_to && filter.hddGb_to < this.filtersDict.hddGb.slice(-1)[0] && server.hddGb > filter.hddGb_to) {
        return false;
      }
      return true;
    });
  }

  applySort(items: Server[], sort: string): Server[] {
    let reverse: boolean = sort.charAt(0) == '-';
    let key: string = reverse ? sort.substr(1) : sort;

    return items.sort((a: Server, b: Server): number => {
      if (a[key] == b[key]) {
        return 0;
      } else if (a[key] < b[key]) {
        return reverse ? 1 : -1;
      } else {
        return reverse ? -1 : 1;
      }
    });
  }

  paginate<T>(items: T[], page: number, perPage: number): Paginator<T> {
    const expectedMax = page * perPage;

    let paginated: Paginator<T> = new Paginator<T>(items.slice((page - 1) * perPage, expectedMax));

    paginated.total = items.length;
    paginated.per_page = perPage;
    paginated.current_page = page;
    paginated.last_page = Math.ceil(paginated.total / paginated.per_page);
    paginated.from = (page - 1) * perPage + 1;
    paginated.to = expectedMax > paginated.total ? paginated.total : expectedMax;

    return paginated;
  }
}
