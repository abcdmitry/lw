import {Location} from "@angular/common"
import {Component, OnInit} from "@angular/core";
import {NavService} from "../_services/nav.service";
import {ActivatedRoute} from "@angular/router";
import {ServersService} from "../_services/servers.service";
import {Observable} from "rxjs/Observable";
import {Server} from "../_entities/server";

@Component({
  styleUrls: ['./server-details.component.scss'],
  template: `
    <div class="row">
      <div class="col s12">
        <ng-template #loading>
          <h3>Loading...</h3>
        </ng-template>
        <div *ngIf="server$ | async as server">
          <h5>{{server.model}} details</h5>
          <table class="servers-list striped highlight">
            <tr><td>Model</td><td>{{server.model}}</td></tr>
            <tr><td>RAM</td><td>{{server.ram}}</td></tr>
            <tr><td>HDD</td><td>{{server.hdd}}</td></tr>
            <tr><td>Location</td><td><a [routerLink]="['../', {location: server.location}]">{{server.location}}</a></td></tr>
            <tr><td>Price</td><td>€{{server.priceEUR}} <span *ngIf="server.currency != '€'" class="green-text">{{server.price}}</span></td></tr>
          </table>
          <br />
          <a (click)="goBack()" class="go-back"><i class="material-icons">arrow_back</i> Back to the servers list</a>
        </div>
      </div>
    </div>
  `
})
export class ServerDetailsComponent implements OnInit {
  server$: Observable<Server>;
  server: Server;

  constructor(
    private navService: NavService,
    private serversService: ServersService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  goBack() {
    this.location.back();
  }

  ngOnInit() {
    this.server$ = this.route.paramMap
      .switchMap(params => {
        const id: number = +params.get('id');
        this.navService.setNav([
          {path: ['../'], title: 'Servers'},
          {path: ['../', id], title: 'Server details'},
        ], this.route);

        return this.serversService.getServer$(id);
      });
  }
}
