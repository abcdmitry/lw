import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from "@angular/core";
import {ServersListFiltersDict} from "../_entities/servers/servers-list-filters-dict";
import {ServersListFilter} from "../_entities/servers/servers-list-filter";
import {Subject} from "rxjs/Subject";
import "rxjs/add/operator/debounceTime";
import "rxjs/add/operator/distinctUntilChanged";
import "rxjs/add/operator/filter";
import {Subscription} from "rxjs/Subscription";
import {ServersService} from "../_services/servers.service";

@Component({
  styleUrls: ['./servers-list-filters.component.scss'],
  selector: 'servers-list-filters',
  template: `
    <div class="row isearch">
      <div class="input-field col s12">
        <input type="text"
               id="filter_freesearch"
               #freeSearchInput
               (keyup)="freeSearch(freeSearchInput.value)"
               [(ngModel)]="filter.free_search"/>
        <label for="filter_freesearch">Instant search</label>
      </div>
    </div>
    <div class="row slider-title">
      <div class="col s12">
        Total storage size
      </div>
    </div>
    <div class="row">
      <div class="col s12 slider-box">
        
        <nouislider 
          [config]="sliderConfig" 
          [(ngModel)]="sliderValue"
          (ngModelChange)="sliderChanged($event)"
        ></nouislider>
      </div>
    </div>
    <div class="row dropdowns">
      <div class="col m4 s12">
        <select name="filter_ram"
                (change)="filterChanged()"
                [(ngModel)]="filter.ramGb"
                [materializeSelectOptions]="filtersDict.ramGb"
                materialize="material_select"
                multiple
        >
          <option value="-1">RAM: any</option>
          <option *ngFor="let item of filtersDict.ramGb" [value]="item">{{item}}GB</option>
        </select>
      </div>
      <div class="col m4 s12">
        <select name="filter_hddType"
                (change)="filterChanged()"
                [(ngModel)]="filter.hddType"
                [materializeSelectOptions]="filtersDict.hddType"
                materialize="material_select"
        >
          <option value="">HDD type: any</option>
          <option *ngFor="let item of filtersDict.hddType" [value]="item">{{item}}</option>
        </select>
      </div>
      <div class="col m4 s12">
        <select name="filter_location"
                (change)="filterChanged()"
                [(ngModel)]="filter.location"
                [materializeSelectOptions]="filtersDict.location"
                materialize="material_select"
        >
          <option value="">Location: any</option>
          <option *ngFor="let item of filtersDict.location" [value]="item">{{item}}</option>
        </select>
      </div>
    </div>
`
})
export class ServersListFiltersComponent implements OnInit, OnDestroy {
  @Output('onFilterChange')
  filterEmitter: EventEmitter<ServersListFilter> = new EventEmitter();

  @Input()
  filter: ServersListFilter = new ServersListFilter();

  private freeSearchSubj = new Subject<string>();
  private freeSearchSubscription: Subscription;

  filtersDict: ServersListFiltersDict;
  private filtersDictSubscription: Subscription;

  sliderConfig: any;
  sliderValue: number[];

  constructor(private serversService: ServersService) {}

  ngOnInit() {
    this.filtersDictSubscription = this.serversService.getFiltersDict$().subscribe(filtersDict => {
      this.filtersDict = filtersDict;
      this.sliderConfig = this.generateSliderConfig();
      this.sliderValue = [
        this.filter.hddGb_from >= 0 ? this.filter.hddGb_from : filtersDict.hddGb[0],
        this.filter.hddGb_to || this.filtersDict.hddGb.slice(-1)[0]
      ];
    });

    this.freeSearchSubscription = this.freeSearchSubj
                                      .debounceTime(300)
                                      .distinctUntilChanged()
                                      .filter(v => v.length > 1 || v.length == 0)
                                      .subscribe(v => this.filterChanged());
  }

  ngOnDestroy() {
    this.freeSearchSubscription.unsubscribe();
    this.filtersDictSubscription.unsubscribe();
  }

  filterChanged() {
    this.filterEmitter.emit(this.validateFilter(this.filter));
  }

  sliderChanged($event) {
    [this.filter.hddGb_from, this.filter.hddGb_to] = this.sliderValue;
    this.filterChanged();
  }

  freeSearch(phrase: string) {
    this.freeSearchSubj.next(phrase);
  }

  /**
   * Unset empty fields of filter
   */
  validateFilter(filter: ServersListFilter): ServersListFilter {
    filter = Object.assign({}, filter);

    for (let i in filter) {
      // If array has values and doesn't contain item with -1 value
      if (Array.isArray(filter[i]) && filter[i].length > 0 && filter[i].indexOf('-1') === -1) {
        continue;
      } else if (typeof filter[i] == 'string' && filter[i].length > 0) {
        continue;
      } else if (typeof filter[i] == 'number' && !isNaN(filter[i]) && filter[i] != Infinity) {
        continue;
      }

      delete filter[i];
    }

    return filter;
  }

  generateSliderConfig(): any {
    let filterMin: number = this.filtersDict.hddGb[0];
    let filterMax: number = this.filtersDict.hddGb.slice(-1)[0];

    let config: any = {
      start: [filterMin, filterMax],
      snap: true,
      connect: true,
      pips: {
        mode: 'values',
        values: this.filtersDict.hddGb,
        density: 8,
        stepped: true,
        format: {
          to: v => v < 1000 ? v + 'GB' : v / 1000 + 'TB'
        }
      }
    };

    config['range'] = {
      min: filterMin,
      max: filterMax,
    };

    let step = Math.floor(100 / (this.filtersDict.hddGb.length - 1));
    for (let i = 1; i < this.filtersDict.hddGb.length - 1; i++) {
      config.range[(i * step) + '%'] = this.filtersDict.hddGb[i];
    }
    return config;
  }
}
