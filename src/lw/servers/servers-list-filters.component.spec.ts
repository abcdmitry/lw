import {ServersListFiltersComponent} from "./servers-list-filters.component";
import {ServersListFilter} from "../_entities/servers/servers-list-filter";
import {ServersService} from "../_services/servers.service";

describe('ServersListFilters component', () => {
  it('should validate filter', () => {
    let testData: {input: ServersListFilter, expected: ServersListFilter}[] = [
      {input: {ramGb: []}, expected: {}},
      {input: {ramGb: [4]}, expected: {ramGb: [4]}},
      {input: {ramGb: [4, 16]}, expected: {ramGb: [4, 16]}},
      {input: {ramGb: [4, 16], hddGb_from: undefined}, expected: {ramGb: [4, 16]}},
      {input: {hddGb_from: 0}, expected: {hddGb_from: 0}},
      {input: {hddGb_from: 0}, expected: {hddGb_from: 0}},
      {input: {hddGb_from: 1/0}, expected: {}},
      {input: {hddGb_from: undefined}, expected: {}},
      {input: {hddGb_from: null}, expected: {}},
      {input: {hddGb_from: NaN}, expected: {}},
    ];
    let component = new ServersListFiltersComponent(<ServersService>({}));

    testData.forEach(
      value => expect(component.validateFilter(value.input)).toEqual(value.expected)
    );
  });
});
