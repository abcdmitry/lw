import {RouterModule, Routes} from "@angular/router";
import {ServersListComponent} from "./servers-list.component";
import {ServerDetailsComponent} from "./server-details.component";
import {NgModule} from "@angular/core";

const routes: Routes = [
  {path: '', component: ServersListComponent},
  {path: ':id', component: ServerDetailsComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ServersRoutingModule {}
