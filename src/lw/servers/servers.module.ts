import {NgModule} from "@angular/core";
import {ServersListComponent} from "./servers-list.component";
import {ServerDetailsComponent} from "./server-details.component";
import {ServersRoutingModule} from "./servers-routing.module";
import {SharedModule} from "../shared/shared.module";
import {ServersListFiltersComponent} from "./servers-list-filters.component";
import {FormsModule} from "@angular/forms";
import {NouisliderModule} from "ng2-nouislider/src/nouislider";

@NgModule({
  imports: [FormsModule, SharedModule, ServersRoutingModule, NouisliderModule],
  declarations: [ServersListComponent, ServersListFiltersComponent, ServerDetailsComponent],
})
export class ServersModule {}
