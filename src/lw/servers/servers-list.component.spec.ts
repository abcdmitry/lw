import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {ServersListComponent} from "./servers-list.component";
import {ServersService} from "../_services/servers.service";
import {NavService} from "../_services/nav.service";
import {Component, Injectable, Input} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {Server} from "../_entities/server";
import {By} from "@angular/platform-browser";
import {ActivatedRoute, Router} from "@angular/router";
import {ActivatedRouteStub, RouterLinkStubDirective, RouterStub} from "../../../testing/router-stubs";
import {Paginator} from "../_entities/paginator";
import {ServersListFilter} from "../_entities/servers/servers-list-filter";

const serversListTestData: Server[] = [
  {id: 1, model: "Dell R210Intel Xeon X3440", ram: "16GBDDR3", hdd: "2x2TBSATA2", location: "AmsterdamAMS-01", price: "€49.99", priceEUR: 49.99, currency: "€"},
  {id: 167, model: "HP DL180 G92x Intel Xeon E5-2620v3", ram: "64GBDDR4", hdd: "2x120GBSSD", location: "Washington D.C.WDC-01", price: "$305.99", priceEUR: 305.99, currency: "€"},
  {id: 236, model: "Dell R6202x Intel Xeon E5-2620v2", ram: "8GBDDR3", hdd: "2x1TBSATA2", location: "SingaporeSIN-11", price: "S$319.99", priceEUR: 100, currency: "S$"},
];

const paginatedServersListStub: Paginator<Server> = {
  total: 3, per_page: 25, current_page: 1, last_page: 1, from: 1, to: 3,
  data: serversListTestData,
};

@Injectable()
class ServersStubService {
  getServers$(): Observable<Paginator<Server>> {
    return Observable.of(paginatedServersListStub);
  }
}

@Injectable()
class NavStubService {
  setNav(nav: any) {};
}

@Component({selector: 'servers-list-filters', template: ''})
class ServersListFiltersStubComponent {
  @Input() filter: ServersListFilter = new ServersListFilter();
}

@Component({selector: 'paginator', template: ''})
class PaginatorStubComponent {
  @Input() paginator: Paginator<Server>;
  @Input() showSummary: boolean;
}

@Component({selector: 'sort-icon', template: ''})
class SortIconStubComponent {
  @Input() field: string;
  @Input() sortedBy: string;
}

describe('ServersList component', () => {
  let component: ServersListComponent;
  let fixture: ComponentFixture<ServersListComponent>;
  let navServiceSpy: jasmine.Spy;
  let navService: NavService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ServersListComponent,
        ServersListFiltersStubComponent,
        PaginatorStubComponent,
        SortIconStubComponent,
        RouterLinkStubDirective
      ],
      providers: [
        {provide: ServersService, useClass: ServersStubService},
        {provide: NavService, useClass: NavStubService},
        {provide: Router, useClass: RouterStub},
        {provide: ActivatedRoute, useClass: ActivatedRouteStub},
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServersListComponent);
    component = fixture.componentInstance;

    navService = fixture.debugElement.injector.get(NavService);
    navServiceSpy = spyOn(navService, 'setNav');
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('renders the servers list', async(() => {
    fixture.detectChanges();

    // All table cells in a flat array
    let values = fixture.debugElement.queryAll(By.css('table.servers-list tr td'));

    // Iterate over the stub array of objects and compare to values obtained from DOM
    for (let i = 0; i < serversListTestData.length; i++) {
      let keys: string[] = Object.keys(serversListTestData[i]);
      keys.splice(keys.indexOf('currency'), 1);
      keys.splice(keys.indexOf('priceEUR'), 1);
      for (let j = 0; j < keys.length; j++) {
        // Content of cell was modified in template
        if (keys[j] == 'price') {
          expect(values[keys.length * i + j].nativeElement.textContent.trim().length).toBeGreaterThanOrEqual(1);
        } else {
          expect(values[keys.length * i + j].nativeElement.textContent.trim()).toEqual(serversListTestData[i][keys[j]].toString());
        }
      }
    }
  }));

  it('should add at least 1 item to the breadcrumbs', async(() => {
    fixture.detectChanges();
    expect(navServiceSpy.calls.mostRecent().args[0].length).toBeGreaterThanOrEqual(1);
  }));
});
