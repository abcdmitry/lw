import {Component, OnInit} from "@angular/core";
import {Server} from "../_entities/server";
import {Observable} from "rxjs/Observable";
import {ServersService} from "../_services/servers.service";
import {NavService} from "../_services/nav.service";
import {Paginator} from "../_entities/paginator";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {ServersListFilter} from "../_entities/servers/servers-list-filter";

@Component({
  styleUrls: ['./servers-list.component.scss'],
  template: `
    <h5>Servers list</h5>
    <ng-template #loading>
      <h3>Loading...</h3>
    </ng-template>
    <div *ngIf="servers$ | async as serversList; else loading">

      <servers-list-filters
        [filter]="filter"
        (onFilterChange)="setFilter($event)"
      ></servers-list-filters>

      <div class="row">
        <div class="col s12">
          <paginator 
            [paginator]="serversList" 
            [showSummary]="true"
            (onSetPage)="setPage($event)"
            (onSetItemsPerPage)="setItemsPerPage($event)"
          ></paginator>
        </div>

        <div class="col s12">
          <table class="servers-list striped highlight">
            <tr>
              <th (click)="setSort('id')" class="sortable"><sort-icon [field]="'id'" [sortedBy]="sort"></sort-icon>ID</th>
              <th (click)="setSort('model')" class="sortable"><sort-icon [field]="'model'" [sortedBy]="sort"></sort-icon>Model</th>
              <th (click)="setSort('ramGb')" class="sortable"><sort-icon [field]="'ramGb'" [sortedBy]="sort"></sort-icon>RAM</th>
              <th (click)="setSort('hddGb')" class="sortable"><sort-icon [field]="'hddGb'" [sortedBy]="sort"></sort-icon>HDD</th>
              <th (click)="setSort('location')" class="sortable"><sort-icon [field]="'location'" [sortedBy]="sort"></sort-icon>Location</th>
              <th (click)="setSort('priceEUR')" class="sortable"><sort-icon [field]="'priceEUR'" [sortedBy]="sort"></sort-icon>Price</th>
            </tr>
            <tr *ngFor="let server of serversList.data" [routerLink]="['../servers', server.id]">
              <td>{{server.id}}</td>
              <td>{{server.model}}</td>
              <td>{{server.ram}}</td>
              <td>{{server.hdd}}</td>
              <td>{{server.location}}</td>
              <td>€{{server.priceEUR.toFixed(2)}} <span *ngIf="server.currency != '€'" class="green-text">{{server.price}}</span></td>
            </tr>
          </table>
        </div>

        <div class="col s12">
          <paginator
            [paginator]="serversList"
            [showSummary]="true"
            (onSetPage)="setPage($event)"
            (onSetItemsPerPage)="setItemsPerPage($event)"
          ></paginator>
        </div>
      </div>
    </div>
  `
})
export class ServersListComponent implements OnInit {
  private page: number = 1;
  private perPage: number;
  sort: string = 'model';
  filter: ServersListFilter;

  servers$: Observable<Paginator<Server>>;
  serversList: Paginator<Server>;

  constructor(
    private serversService: ServersService,
    private navService: NavService,
    private route: ActivatedRoute,
    private router: Router,
  ) {}

  ngOnInit() {
    this.navService.setNav([{path: ['../servers'], title: 'Servers'}], this.route);

    this.servers$ = this.route.paramMap
      .switchMap((params: ParamMap) => {
        this.page = +params.get('page') || 1;
        this.perPage = +params.get('perPage') || 25;
        this.sort = params.get('sort') || 'model';

        this.filter = new ServersListFilter();
        ['ramGb', 'hddGb_from', 'hddGb_to', 'hddType', 'location', 'free_search'].map(key => {
          if (params.has(key)) {
            if (key == 'ramGb') {
              // TODO: It's either a router bug, or passing array among params is misuse of router.navigate()
              // On page reload router can't split comma-separated values into array
              // Although on router.navigate everything works as expected
              // Anyway code below resolves the issue
              let parseAttempt: number[] = params.get('ramGb')
                .toString()
                .split(',')
                .map(value => +value);
              this.filter['ramGb'] = parseAttempt.length > 1 ? parseAttempt : params.getAll('ramGb').map(value => +value);
            } else if (key == 'hddGb_from' || key == 'hddGb_to') {
              this.filter[key] = +params.get(key);
            } else {
              this.filter[key] = params.get(key);
            }
          }
        });

        return this.serversService.getServers$(this.page, this.perPage, this.filter, this.sort);
      });
  }

  setSort(sort: string) {
    if (this.sort == '-' + sort || this.sort != sort) {
      this.sort = sort;
    } else {
      this.sort = '-' + sort;
    }
    this.router.navigate(['./', Object.assign({page: 1, perPage: this.perPage, sort: this.sort}, this.filter)], {relativeTo: this.route});
  }

  setPage($event: number) {
    this.router.navigate(['./', Object.assign({page: $event, perPage: this.perPage, sort: this.sort}, this.filter)], {relativeTo: this.route});
  }

  setItemsPerPage($event: number) {
    this.router.navigate(['./', Object.assign({page: 1, perPage: $event, sort: this.sort}, this.filter)], {relativeTo: this.route});
  }

  setFilter($event: ServersListFilter) {
    this.router.navigate(['./', Object.assign({page: 1, perPage: this.perPage, sort: this.sort}, $event)], {relativeTo: this.route});
  }
}
