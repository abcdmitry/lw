export class Paginator<T> {
    total: number = 0;
    per_page: number = 25;
    current_page: number = 1;
    last_page: number = 1;
    from: number;
    to: number;
    data: Array<T>;

    constructor(data: Array<T>) {
        this.data = data;
    }
}
