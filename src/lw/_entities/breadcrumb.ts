export class Breadcrumb {
  /**
   * Array of commands for Router.navigate()
   */
  path: any[];
  /**
   * Title for the link
   */
  title: string;
  /**
   * Compiled url string. Compiled automatically by NavService
   */
  fullUrl?: string;
}
