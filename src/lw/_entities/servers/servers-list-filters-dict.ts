export class ServersListFiltersDict {
  ramGb: number[];
  hddGb: number[];
  hddType: string[];
  location: string[];
  free_search?: string;
}
