export class ServersListFilter {
  ramGb?: number[];
  hddGb_from?: number;
  hddGb_to?: number;
  hddType?: string;
  location?: string;
  free_search?: string;
}
