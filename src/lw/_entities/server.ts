export class Server {
  id: number;
  model: string;
  ram: string;
  hdd: string;
  location: string;
  price: string;

  // Auxiliary data filled upon receiving backend response
  searchString?: string;
  hddType?: string;
  hddGb?: number;
  ramGb?: number;
  priceEUR?: number;
  currency?: string;
}
